from django.apps import AppConfig


class TrackrecordsConfig(AppConfig):
    name = 'trackrecords'
