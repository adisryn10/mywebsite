from django.conf.urls import url
from django.urls import path
from .views import index
from .views import delete_schedule
from . import views

urlpatterns = [
    url(r'^$', index, name='index'),
    path('addschedule/', views.addschedule, name='addschedule'),
    path('resultschedule/', views.index, name='resultschedule'),
    path('deleteschedule/', delete_schedule, name = 'deleteschedule'),
]
