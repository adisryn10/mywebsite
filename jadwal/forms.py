from django import forms
from .models import Schedule
from django.forms import ModelForm

class Message_Form(forms.ModelForm):
##    error_messages = {
##        'required': 'Please type',
##    }
##    pos_attrs = {
##        'type': 'text',
##        'class': 'todo-form-input',
##        'placeholder':'Your position'
##    }
##    title_attrs = {
##        'type': 'text',
##        'class': 'todo-form-input',
##        'placeholder':'Title of the project'
##    }
##    description_attrs = {
##        'type': 'text',
##        'cols': 50,
##        'rows': 4,
##        'class': 'todo-form-textarea',
##        'placeholder':'Description of the project'
##    }


    name = forms.CharField(label='Nama kegiatan\t',
                           required=True, max_length=50,
                           widget = forms.TextInput())
    date = forms.DateTimeField(label='Date\t',
                           required = True,
                           widget = forms.DateInput())
    place = forms.CharField(label='Place\t',
                            required = True,max_length=50,
                            widget = forms.TextInput())
    category = forms.CharField(label='Category\t',
                              required=True,
                              widget=forms.Textarea())
    class Meta :
        model = Schedule
        fields = ("name","date","place","category")
