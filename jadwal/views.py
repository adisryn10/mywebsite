from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from . import forms
from .forms import Message_Form
from .models import Schedule

response = {}

def index(request):
    schedule = Schedule.objects.all()
    response['schedule'] = schedule
    html = 'resultschedule.html'
    return render(request, html, response)

def delete_schedule(request):
    Schedule.objects.all().delete()
    return redirect('/resultschedule/')

##def addschedule(request):
##    response['title'] = 'Add Schedule'
##    response['addschedule_form'] = Message_Form
##    html = 'addschedule.html'
##    return render(request,html,response)
##
##def resultschedule(request):
##    form = Message_Form(request.POST)
##    if(request.method == 'POST' and form.is_valid()):
##        response['name'] = request.POST['name']
##        response['date'] = request.POST['date']
##        response['place'] = request.POST['place']
##        response['category'] = request.POST['category']
##        schedule = Schedule(name = response['date'], date = response['date'],
##                            category = response['category'])
##        schedule.save()
##        
##        return HttpResponseRedirect('/')
##    else:
##        return HttpResponseRedirect('/')
# Create your views here.
def addschedule(request):
    form = forms.Message_Form(request.POST)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/')
    else:
        form = forms.Message_Form()

    return render(request, 'addschedule.html', {'form':form})

##def resultschedule(request):
##    schedule_list = Schedule.objects.order_by('date')
##    schedule_dict = {'schedule':schedule_list}
##    return render(request, 'resultschedule.html', context=schedule_dict)
