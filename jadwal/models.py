from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Schedule(models.Model):
    name = models.CharField(max_length=27)
    date = models.DateTimeField(default = timezone.now)
    place = models.CharField(max_length=27)
    category = models.TextField()
